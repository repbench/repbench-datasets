import zipfile
import bz2
import numpy as np

import qmmlpack as qmml
from cmlkit import Dataset, Subset

from iterate import each_outer, each_inner, each_inner_per_outer


def load_data_splits(filename):
    with open(filename, "rt") as f:
        raw = f.read()
    res = {}
    for line in raw.split("\n"):
        if len(line.strip()) == 0:
            continue
        key, data = line.split(":")
        data = np.asarray([int(i) for i in data.split()], dtype=int)
        res[key] = data

    return res


def find_a_in_b(a, b):
    la = list(a)
    lb = list(b)
    return np.array([lb.index(ib) for ib in la], dtype=int)


gdb9file = "gdb9-14b.zip"
splitfile = "qm9-datasplits.txt"

# data split format:
# ov-{i} -> experiment reroll i (we just redo everything)
# ot-{i}-{n} -> training set of size n for experiment i
# iv-{i}-{n}-{k} -> validation portion of train/test split k of training set of size n of experiment i
# currently we have 10 ov; and 10 iv
# the splits have been generated externally, they exclude 3054 uncharacterized mols
# and small molecules (see TODO: add ipynb)
# the .zip file can be obtained from qmml.org

idx = load_data_splits(splitfile)

for i in each_inner():
    idx[i["name_it"]] = np.setdiff1d(idx[i["name_ot"]], idx[i["name_iv"]])
    assert i["name_iv"] in idx
    assert i["name_it"] in idx
    assert i["name_ov"] in idx
    assert i["name_ot"] in idx

# now generate *relative* indices
# so the inner splits can be proper Subsets of
# the outer training set (needed to make CV work correctly)
print("qm9: Generating relative indices, this will take a while...")
for i in each_inner():
    ot = idx[i["name_ot"]]
    iv = idx[i["name_iv"]]
    it = idx[i["name_it"]]

    idx[i["name_iv"] + "-rel"] = find_a_in_b(iv, ot)
    idx[i["name_it"] + "-rel"] = find_a_in_b(it, ot)


with zipfile.ZipFile(gdb9file) as zf:  # access the downloaded .zip file
    with zf.open("dsC7O2H10nsd.xyz.bz2") as f:
        isomers = bz2.decompress(
            f.read()
        )  # isomers.decode(encoding='ascii') yields .xyz file as string
        isomers = qmml.import_extxyz(
            isomers, additional_properties=True
        )  # parsed data structure
    with zf.open("dsgdb9nsd.xyz.bz2") as f:
        mols = bz2.decompress(
            f.read()
        )  # mols.decode(encoding='ascii') yields .xyz file as string
        mols = qmml.import_extxyz(
            mols, additional_properties=True
        )  # parsed data structure

# auxiliary files
with zipfile.ZipFile(gdb9file) as zf:  # access the downloaded .zip file
    with zf.open("atomref.txt") as f:
        atomref = f.read().decode(encoding="ascii")


z = mols["atomic_numbers"]
r = mols["coordinates"]
n_atoms = np.array([len(zz) for zz in z])

propU0 = np.asfarray([mp[12] for mp in mols["system_properties"]])
propU = np.asfarray([mp[13] for mp in mols["system_properties"]])
propH = np.asfarray([mp[14] for mp in mols["system_properties"]])
propG = np.asfarray([mp[15] for mp in mols["system_properties"]])

# subtract linear part
atomrefval = [line.split() for line in atomref.split("\n")[5:-2]]
atomrefval = np.asfarray([[float(entry) for entry in line[1:]] for line in atomrefval])

t = [None, 0, None, None, None, None, 1, 2, 3, 4]
for i in range(len(mols["atomic_numbers"])):  # i is molecule index
    for j in mols["atomic_numbers"][i]:  # j is atom in molecule i
        propU0[i] -= atomrefval[t[j], 1]
        propU[i] -= atomrefval[t[j], 2]
        propH[i] -= atomrefval[t[j], 3]
        propG[i] -= atomrefval[t[j], 4]

# Hartree to kcal/mol
propU0 *= 627.509_474_277_194
propU *= 627.509_474_277_194
propH *= 627.509_474_277_194
propG *= 627.509_474_277_194

print("qm9: Finished loading.")


master = Dataset(
    z=z,
    r=r,
    p={
        "u0": propU0 / n_atoms,
        "u": propU / n_atoms,
        "h": propH / n_atoms,
        "g": propG / n_atoms,
    },
    name="qm9",
    desc="QM9 (GDB9-14) full dataset: 134k small organic mols, in their ground states, with energetic, electronic and thermodynamic properties. All properties have been normalised per atom and are given in kcal/mol.",
)

master.save(directory="../datasets")

print("qm9: Saved master dataset.")

for o in each_outer():
    # collect inner splits
    ot_splits = np.array(
        [
            [idx[i["name_it"] + "-rel"], idx[i["name_iv"] + "-rel"]]
            for i in each_inner_per_outer(o)
        ],
        dtype=object,
    )

    for it, iv in ot_splits:
        assert len(it) + len(iv) == o["n"]
        assert len(it) > len(iv)

    ot = Subset.from_dataset(
        master,
        idx=idx[o["name_ot"]],
        name="qm9-" + o["name_ot"],
        desc=f"QM9 (GDB9-14), {o['name_ot']}",
        splits=ot_splits,
    )
    ot.save(directory="../datasets/")
    ov = Subset.from_dataset(
        master,
        idx=idx[o["name_ov"]],
        name="qm9-" + o["name_ov"],
        desc=f"QM9 (GDB9-14), {o['name_ov']}",
    )
    ov.save(directory="../datasets/")

print("qm9: Done!")
