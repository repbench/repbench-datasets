# GDB9-14 Dataset

The citation for this dataset is:

> Raghunathan Ramakrishnan, Pavlo Dral, Matthias Rupp, O. Anatole von Lilienfeld: Quantum Chemistry Structures and Properties of 134 kilo Molecules, Scientific Data 1: 140022, 2014. [DOI](https://doi.org/10.1038/sdata.2014.22)

For stratification, we used:

- Atomisation energy
- Number of molecules with 7, 8, 9 non-H atoms (**smaller molecules were excluded**)
- Number of atoms of each non-H element (N, O, F)
- Number of atoms (including H)

For details, check the notebook, or have a look at [the paper](https://marcel.science/repbench)!
