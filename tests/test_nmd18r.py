import cmlkit

# these hashes match the datasets used for the repbench paper,
# they should match the re-generated datasets.

# property hashes used here are obtained by
# cmlkit.engine.compute_hash(np.round(d.p[PROP], 16))
# on the repbench datasets, otherwise they don't exactly
# match up due to floating point issues during import/export


def test_nmd18r_master():
    d = cmlkit.load_dataset("../datasets/nmd18r.npy")

    assert d.name == "nmd18r"

    assert (
        cmlkit.engine.compute_hash(d.p["fe"]) == "c4b9c1e0bd16ddd3fa1b4a56854ef0bc"
    )
    assert (
        cmlkit.engine.compute_hash(d.p["bg"]) == "a4941eb3cbda43d1ee633195fc3a6576"
    )
    assert (
        cmlkit.engine.compute_hash(d.p["sg"]) == "0446dd7e067cfa2620caee031835a7db"
    )
    assert d.geom_hash == "7e92f380883ddc9b937f0a63fbe4f7da"
    assert d.b[69][2][1] == -0.049224
    assert d.pp("fe", per="atom")[42] == 0.11504


def test_nmd18r_ot():
    d = cmlkit.load_dataset("../datasets/nmd18r-ot-4-160")

    assert d.name == "nmd18r-ot-4-160"

    assert d.geom_hash == "13fe0c3e2bb1ab6e448f913ef447fc4e"
    assert d.r[99][0][0] == 0.00022033

    assert (
        cmlkit.engine.compute_hash(d.p["fe"]) == "e578dc9c572d44c5593d762fd4bc35df"
    )


def test_nmd18r_ov():
    d = cmlkit.load_dataset("../datasets/nmd18r-ov-5")

    assert d.name == "nmd18r-ov-5"

    assert d.geom_hash == "427f182685b8b77936e7d5cdc4fdca75"
    assert d.r[81][2][1] == -0.00506602

    assert (
        cmlkit.engine.compute_hash(d.p["fe"]) == "63fb0f2140613ba2294e4f438a6793d7"
    )
