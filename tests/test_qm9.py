import cmlkit

# these hashes match the datasets used for the repbench paper,
# they should match the re-generated datasets.


def test_qm9_master():
    d = cmlkit.load_dataset("../datasets/qm9.npy")

    assert d.name == "qm9"

    assert d.hash == "5dc48c6fa749550b57fc256b6b4932e0"
    assert d.geom_hash == "a8dbd7f006c5dd887ef9aceab92ea411"
    assert d.r[42][2][2] == -0.2492041635
    assert d.pp("u0", per="mol")[8] == -670.2685973639145


def test_qm9_ot():
    d = cmlkit.load_dataset("../datasets/qm9-ot-4-100")

    assert d.name == "qm9-ot-4-100"

    assert d.hash == "b8dcb020f93d45863849a877e4cef249"
    assert d.geom_hash == "0e87d58e15c0f9e14bc4dd092b016eb7"
    assert d.r[81][2][1] == -0.9669975441


def test_qm9_ov():
    d = cmlkit.load_dataset("../datasets/qm9-ov-6")

    assert d.name == "qm9-ov-6"

    assert d.hash == "96fed7fcf4ef4d37259f07398055ff29"
    assert d.geom_hash == "6481094f3659dbab477cb8c03c852e6d"
    assert d.r[81][2][1] == -0.6867744828
