import numpy as np
import cmlkit

# these hashes match the datasets used for the repbench paper,
# they should match the re-generated datasets.

# property hashes used here are obtained by
# cmlkit.engine.compute_hash(np.round(d.p[PROP], 16))
# on the repbench datasets, otherwise they don't exactly
# match up due to floating point issues during import/export


def test_nmd18u_master():
    d = cmlkit.load_dataset("../datasets/nmd18u.npy")

    assert d.name == "nmd18u"

    assert (
        cmlkit.engine.compute_hash(d.p["fe"]) == "c4b9c1e0bd16ddd3fa1b4a56854ef0bc"
    )
    assert (
        cmlkit.engine.compute_hash(d.p["bg"]) == "a4941eb3cbda43d1ee633195fc3a6576"
    )
    assert (
        cmlkit.engine.compute_hash(d.p["sg"]) == "0446dd7e067cfa2620caee031835a7db"
    )

    assert d.geom_hash == "782c9a6be7e5699431a23e9833aa4290"
    assert d.b[69][2][1] == -0.0986923
    assert d.pp("fe", per="atom")[42] == 0.11504


def test_nmd18u_ot():
    d = cmlkit.load_dataset("../datasets/nmd18u-ot-4-160")

    assert d.name == "nmd18u-ot-4-160"

    assert d.geom_hash == "7e6bf5b6779618f37f37007897f6e638"
    assert d.r[99][0][0] == -0.00192837512364

    assert (
        cmlkit.engine.compute_hash(d.p["fe"]) == "e578dc9c572d44c5593d762fd4bc35df"
    )


def test_nmd18u_ov():
    d = cmlkit.load_dataset("../datasets/nmd18u-ov-5")

    assert d.name == "nmd18u-ov-5"

    assert d.geom_hash == "3da8636c4b15ece5260e0173801ba866"
    assert d.r[81][2][1] == 0.0137102841

    assert (
        cmlkit.engine.compute_hash(d.p["fe"]) == "63fb0f2140613ba2294e4f438a6793d7"
    )
