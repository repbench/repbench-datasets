import cmlkit

# these hashes match the datasets used for the repbench paper,
# they should match the re-generated datasets.


def test_ba10_master():
    d = cmlkit.load_dataset("../datasets/ba10.npy")

    assert d.name == "ba10"

    assert d.hash == "06140af3ab4c14533c0fd9cddb6c516a"
    assert d.geom_hash == "5daf67e6647258116e84647ec84f98f1"
    assert d.r[123][1][1] == 3.7
    assert d.pp("fe", per="cell")[42] == 0.860225


def test_ba10_ot():
    d = cmlkit.load_dataset("../datasets/ba10-ot-1-100.npy")

    assert d.name == "ba10-ot-1-100"

    assert d.hash == "1d28a0d7390fc147833cd0d468c9af5f"
    assert d.geom_hash == "e94c5fea162b28dab073ce0317da7ca3"

    assert d.r[2][2][1] == -2.8
    assert d.splits[2][1][3] == 9


def test_ba10_ov():
    d = cmlkit.load_dataset("../datasets/ba10-ov-3.npy")

    assert d.name == "ba10-ov-3"

    assert d.hash == "8a20d56c7b55b8d3d4ee5711bb451de1"
    assert d.geom_hash == "d45486fdb855def39728ed1fc999d4b9"

    assert d.r[3][1][1] == -1.385
