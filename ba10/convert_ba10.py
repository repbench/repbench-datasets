import zipfile

import numpy as np

import qmmlpack as qmml
import cmlkit as cml

from iterate import each_outer, each_inner, each_inner_per_outer


def load_data_splits(filename):
    with open(filename, "rt") as f:
        raw = f.read()
    res = {}
    for line in raw.split("\n"):
        if len(line.strip()) == 0:
            continue
        key, data = line.split(":")
        data = np.asarray([int(i) for i in data.split()], dtype=int)
        res[key] = data

    return res


def find_a_in_b(a, b):
    la = list(a)
    lb = list(b)
    return np.array([lb.index(ib) for ib in la], dtype=int)


ba10file = "ba10-18.zip"
splitfile = "ba10-18-datasplits.txt"

# data split format:
# ov-{i} -> experiment reroll i (we just redo everything)
# ot-{i}-{n} -> training set of size n for experiment i
# iv-{i}-{n}-{k} -> validation portion of train/test split k of training set of size n of experiment i
# currently we have 10 ov; and 10 iv

idx = load_data_splits(splitfile)

for i in each_inner():
    idx[i["name_it"]] = np.setdiff1d(idx[i["name_ot"]], idx[i["name_iv"]])
    assert i["name_iv"] in idx
    assert i["name_it"] in idx
    assert i["name_ov"] in idx
    assert i["name_ot"] in idx


# now generate *relative* indices
# so the inner splits can be proper Subsets of
# the outer training set (needed to make CV work correctly)
print("ba10: Generating relative indices, this will take a while...")
for i in each_inner():
    ot = idx[i["name_ot"]]
    iv = idx[i["name_iv"]]
    it = idx[i["name_it"]]

    idx[i["name_iv"] + "-rel"] = find_a_in_b(iv, ot)
    idx[i["name_it"] + "-rel"] = find_a_in_b(it, ot)


with zipfile.ZipFile(ba10file) as zf:  # access the downloaded .zip file
    with zf.open("ba10-18.xyz") as f:
        alloys = f.read().decode("ascii")
        alloys = qmml.import_extxyz(
            alloys, additional_properties=True
        )  # parsed data structure
    with zf.open("readme.txt") as f:
        readme = f.read().decode("ascii")

z = alloys["atomic_numbers"]
r = alloys["coordinates"]
n_atoms = np.array([len(zz) for zz in z])

# sizes of unit cells
# alloys are not saturated, and there is therefore no difference between
# sizes with and without hydrogens
sizes = np.asarray([len(an) for an in alloys["atomic_numbers"]])

# lattice type
lattice = np.asarray([sp[1] for sp in alloys["system_properties"]])

# Formation Energy / eV
propFE = np.asfarray([sp[3] for sp in alloys["system_properties"]])

# basis
basis = np.asfarray(alloys["additional_properties"])  # n x 3 x 3
alloys["basis"] = basis


print("ba10: Finished loading data.")

# collect master splits
# master_splits = np.array(
#     [[idx[o["name_ot"]], idx[o["name_ov"]]] for o in each_outer()], dtype=object
# )

master = cml.Dataset(
    z,
    r,
    b=basis,
    p={"fe": propFE / n_atoms},
    name="ba10",
    desc="ba10-18 full dataset: Energies of 10 binary alloys with 1,595 structures each. 10 binary alloys (AgCu, AlFe, AlMg, AlNi, AlTi, CoNi, CuFe, CuNi, FeV, NbNi) with 10 different species and all possible FCC, BCC and HCP structures up to 8 atoms in the unit cell. 15,950 structures in total. Lattice parameters from Vegard's rule. DFT/PBE formation energies in eV/atom.",
    # splits=master_splits,
)

master.save(directory="../datasets/")
# print(f"{master.info['geometry']['min_dist']:.3f} - {master.info['geometry']['max_dist']:.3f}")

print("ba10: Created the overall dataset.")
print("ba10: Now creating the subsets...")

for o in each_outer():

    # collect inner splits
    ot_splits = np.array(
        [
            [idx[i["name_it"] + "-rel"], idx[i["name_iv"] + "-rel"]]
            for i in each_inner_per_outer(o)
        ],
        dtype=object,
    )

    for it, iv in ot_splits:
        assert len(it) + len(iv) == o["n"]
        assert len(it) > len(iv)

    ot = cml.Subset.from_dataset(
        master,
        idx[o["name_ot"]],
        name="ba10-" + o["name_ot"],
        desc=f"ba10, {o['name_ot']}",
        splits=ot_splits,
    )
    assert ot.n == o["n"]
    ot.save(directory="../datasets/")
    ov = cml.Subset.from_dataset(
        master,
        idx[o["name_ov"]],
        name="ba10-" + o["name_ov"],
        desc=f"ba10, {o['name_ov']}",
    )
    assert ov.n == 1000
    ov.save(directory="../datasets/")


print(
    "ba10: We're done. All subsets have passed very basic consistency checks, and everything has been saved. Have a good day!"
)
