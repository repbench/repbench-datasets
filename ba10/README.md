# BA10-18 Dataset

The citation for this dataset is:
> Chandramouli Nyshadham, Matthias Rupp, Brayden Bekker, Alexander V. Shapeev, Tim Mueller, Conrad W. Rosenbrock, Gábor Csányi, David W. Wingate, Gus L. W. Hart: General Machine-Learning Surrogate Models for Materials Prediction, *npj Computational Materials* **5**, 51 (2019). [URL](https://doi.org/10.1038/s41524-019-0189-9)

For the stratification, the following quantities were taken into account:
- Formation energy
- Elemental composition (i.e. number of atoms of element)
- Number of atoms in unit cell (unit cells up to `n=5` were **excluded**)

For details, please see the notebook and the `repbench` paper.
