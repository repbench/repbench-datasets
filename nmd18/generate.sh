#!/bin/sh

curl -O https://qmml.org/Datasets/nmd-18.zip
python convert_nmd18u.py
python convert_nmd18r.py
rm nmd-18.zip