trainsetsizes = [100, 160, 250, 400, 650, 1000, 1600]
numov, numiv = 10, 10


def each_inner(trainsetsizes=trainsetsizes, numov=numov, numiv=numiv):
    for outer in each_outer(trainsetsizes=trainsetsizes, numov=numov):
        for i_iv in range(1, numiv + 1):
            name_iv = f"iv-{outer['i_ov']}-{outer['n']}-{i_iv}"
            name_it = f"it-{outer['i_ov']}-{outer['n']}-{i_iv}"

            yield {**outer, **{"name_iv": name_iv, "name_it": name_it, "i_iv": i_iv}}


def each_outer(trainsetsizes=trainsetsizes, numov=numov):
    for i_ov in range(1, numov + 1):
        for n in trainsetsizes:
            name_ot = f"ot-{i_ov}-{n}"
            name_ov = f"ov-{i_ov}"

            yield {"name_ot": name_ot, "name_ov": name_ov, "i_ov": i_ov, "n": n}


def each_inner_per_outer(outer, numiv=numiv):
    for i_iv in range(1, numiv + 1):
        name_iv = f"iv-{outer['i_ov']}-{outer['n']}-{i_iv}"
        name_it = f"it-{outer['i_ov']}-{outer['n']}-{i_iv}"

        yield {**outer, **{"name_iv": name_iv, "name_it": name_it, "i_iv": i_iv}}
