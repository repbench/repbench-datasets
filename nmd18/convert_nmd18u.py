import zipfile

import numpy as np

import qmmlpack as qmml
import cmlkit as cml

from iterate import each_outer, each_inner, each_inner_per_outer


def load_data_splits(filename):
    with open(filename, "rt") as f:
        raw = f.read()
    res = {}
    for line in raw.split("\n"):
        if len(line.strip()) == 0:
            continue
        key, data = line.split(":")
        data = np.asarray([int(i) for i in data.split()], dtype=int)
        res[key] = data

    return res


def find_a_in_b(a, b):
    la = list(a)
    lb = list(b)
    return np.array([lb.index(ib) for ib in la], dtype=int)


nmdfile = "nmd-18.zip"
splitfile = "nmd18-datasplits.txt"

# data split format:
# ov-{i} -> experiment reroll i (we just redo everything)
# ot-{i}-{n} -> training set of size n for experiment i
# iv-{i}-{n}-{k} -> validation portion of train/test split k of training set of size n of experiment i
# currently we have 10 ov; and 10 iv

idx = load_data_splits(splitfile)

for i in each_inner():
    idx[i["name_it"]] = np.setdiff1d(idx[i["name_ot"]], idx[i["name_iv"]])
    assert i["name_iv"] in idx
    assert i["name_it"] in idx
    assert i["name_ov"] in idx
    assert i["name_ot"] in idx


# now generate *relative* indices
# so the inner splits can be proper Subsets of
# the outer training set (needed to make CV work correctly)
print("nmd18u: Generating relative indices, this will take a while...")
for i in each_inner():
    ot = idx[i["name_ot"]]
    iv = idx[i["name_iv"]]
    it = idx[i["name_it"]]

    idx[i["name_iv"] + "-rel"] = find_a_in_b(iv, ot)
    idx[i["name_it"] + "-rel"] = find_a_in_b(it, ot)


with zipfile.ZipFile(nmdfile) as zf:  # access the downloaded .zip file
    with zf.open("nmd18u.xyz") as f:
        xyz = f.read().decode("ascii")
        xyz = qmml.import_extxyz(xyz, additional_properties=True)  # parsed data structure

prop = np.array(xyz["system_properties"])

print("nmd18u: Finished loading data.")


master = cml.Dataset(
    z=xyz["atomic_numbers"],
    r=xyz["coordinates"],
    b=xyz["additional_properties"],
    p={
        "fe": np.round(prop[:, 0], 16),
        "bg": np.round(prop[:, 1], 16),
        "sg": prop[:, 2].astype(int),
    },
    name="nmd18u",
    desc="Full dataset from the NOMAD2018 Kaggle challenge.\n3000 group-III oxides with formation energy (fe) (eV/atom) and bandgap (bg) (eV/structure).\nSpacegroup (sg) is also given.\nGeometries are Vegard's rule geometries,\nbut properties are computed for the relaxed geometries.",
)


master.save(directory="../datasets/")

print("nmd18u: Created the overall dataset.")
print("nmd18u: Now creating the subsets...")

for o in each_outer():

    # collect inner splits
    ot_splits = np.array(
        [
            [idx[i["name_it"] + "-rel"], idx[i["name_iv"] + "-rel"]]
            for i in each_inner_per_outer(o)
        ],
        dtype=object,
    )

    for it, iv in ot_splits:
        assert len(it) + len(iv) == o["n"]
        assert len(it) > len(iv)

    ot = cml.Subset.from_dataset(
        master,
        idx[o["name_ot"]],
        name="nmd18u-" + o["name_ot"],
        desc=f"nmd18u, {o['name_ot']}",
        splits=ot_splits,
    )
    assert ot.n == o["n"]
    ot.save(directory="../datasets/")
    ov = cml.Subset.from_dataset(
        master,
        idx[o["name_ov"]],
        name="nmd18u-" + o["name_ov"],
        desc=f"nmd18u, {o['name_ov']}",
    )
    assert ov.n == 600
    ov.save(directory="../datasets/")

# sanity check:
# The first five band gaps of iv-7-650-9 are 0.7819, 0.367, 0.8366, 1.7153, 2.7282
# The first five space groups of ot-3-1000 are 12, 194, 12, 227, 33

tmp = cml.load_dataset("../datasets/nmd18u-ot-7-650.npy")
test = cml.Subset.from_dataset(tmp, idx=tmp.splits[8][1])
assert test.p["bg"][0] == 0.7819
assert test.p["bg"][1] == 0.367
assert test.p["bg"][2] == 0.8366
assert test.p["bg"][3] == 1.7153
assert test.p["bg"][4] == 2.7282

test = cml.load_dataset("../datasets/nmd18u-ot-3-1000.npy")
assert test.p["sg"][0] == 12
assert test.p["sg"][1] == 194
assert test.p["sg"][2] == 12
assert test.p["sg"][3] == 227
assert test.p["sg"][4] == 33


print(
    "nmd18u: We're done. All subsets have passed very basic consistency checks, and everything has been saved. Have a good day!"
)
