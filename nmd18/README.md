# nmd18: nmd18u and nmd18r

The [Nomad2018 Kaggle challenge](https://www.kaggle.com/c/nomad2018-predict-transparent-conductors) dataset, with projected (Vegard's Rule) geometries, as used in the original challenge, and relaxed geometries.

Citation for this dataset:

> Christopher Sutton, Luca M. Ghiringhelli, Takenori Yamamoto, Yury Lysogorskiy, Lars Blumenthal, Thomas Hammerschmidt, Jacek R. Golebiowski, Xiangyue Liu, Angelo Ziletti & Matthias Scheffler: Crowd-sourcing materials-science challenges with the NOMAD 2018 Kaggle competition. *npj Computational Materials* **5**, 111 (2019). [DOI](https://doi.org/10.1038/s41524-019-0239-3)

For stratification, we used:

- Formation energy
- Number of atoms in unit cell
- Number of atoms of each element

For details, please see the notebook and the paper!
