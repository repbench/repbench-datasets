# repbench-datasets

This repository contains all the scripts and data needed to generate the datasets used in the [`repbench`](https://marcel.science/repbench) project, allowing you to reproduce its results, and compare your own models in an identical setting. 

**Notice: I do not assert any copyright over the underlying datasets. Please make sure to cite the original works and the [`repbench`](https://marcel.science/repbench) paper when using the datasets provided here.**

In particular, this repository will generate:

- The full `qm9`, `ba10`, `nmd18u` and `nmd18r` datasets in [`cmlkit`](https://marcel.science/cmlkit) format.
- For each dataset and different `N_train`: 10 outer train/test splits (test splits are constant across `N_train`).
- For each dataset and different `N_train` and different training set: 10 *inner* 80%/20% splits for hyper-parameter optimisation with cross-validation.
- All splits are *stratified* according to multiple criteria, i.e. they are randomly sampled, but in a way that ensures that the statistics of selected properties are similar across the splits.

The datasets are the following:

- `qm9`, also known as `gdb9` or `gdb9-14`; 134k small organic molecules in their ground states, by [Ramakrishnan *et al.* (2014)](https://doi.org/10.1038/sdata.2014.22). Here, we provide the energy-related properties (`u0`, `h`, `g`, `u`) only. Training set sizes are: `[100, 250, 650, 1600, 4000, 10000]`. Test set size is: `10000`.
- `ba10`, also known as `ba10-18` or `DFT-10B`; energies of 10 binary alloys with 1595 (unrelaxed) structures each, by [Nyhadham *et al.* (2019)](https://doi.org/10.1038/s41524-019-0189-9). Only the `fe` property, the formation energy, is provided. Training set sizes are: `[100, 250, 650, 1600, 4000, 10000]`. Test set size is: `1000`.
- `nmd18`, the dataset used in the [Nomad2018 Predicting Transparent Conductors Kaggle Challenge](https://www.kaggle.com/c/nomad2018-predict-transparent-conductors). 3000 `AlGaIn` ternary oxides, with formation energy (`fe`) and bandgap (`bg`) corresponding to their relaxed structures. The `nmd18u` dataset contains *unrelaxed* geometries (obtained with Vegard's Rule), and `nmd18r` (or `xnmd18`) the corresponding *relaxed* geometries. Dataset by [Sutton *et al.* (2019)](https://doi.org/10.1038/s41524-019-0239-3). Training set size are: `[100, 160, 250, 400, 650, 1000, 1600]`. Test set size is: `600`.

Please refer to [qmml.org](https://qmml.org) for further information and the datasets in a more universal format.

## Using this repository

### Requirements

In order for the scripts to run, you need:

- `cmlkit` (with `qmmlpack`)
- `pytest` to run the consistency checks
- `curl` and `sh`, which should be always present on a unix-ish system

You also need to clone this repository somewhere first. Make sure the disk has at least 1GB of free space.

### Generating the datasets

It is recommended that you generate the datasets yourself by running

```
sh generate.sh
```

This will automatically download the base data from [qmml.org](https://qmml.org), convert to [`cmlkit`](https://marcel.science/cmlkit) format, and run checks. It will take a fair amount of time, so make yourself a nice cup of coffee and then go for a walk. In the end, this repository will take up to 900MB of space. 

The result will be a `datasets/` folder with the `cmlkit` datasets. If you add it to `CML_DATASET_PATH`, these datasets will be available via `cmlkit.load_dataset()`.

The script will also run basic consistency checks in order to check that the datasets are identical to the ones used in `repbench`. If the tests fail, please get in touch with me so we can figure out what's going on.

The successful output of the test suite looks like this:

```
test_ba10.py ...      [ 25%]
test_nmd18r.py ...    [ 50%]
test_nmd18u.py ...    [ 75%]
test_qm9.py ...       [100%]
```

You may also run the `generate.sh` scripts in the individual dataset folders if you only need certain datasets. Please manually run the corresponding test with `cd ../tests; pytest test_NAME.py` to verify that everything worked.

In emergencies, you may also download the converted datasets from `https://data.marcel.science/repbench-datasets-v1.tar.gz` and extract them with `tar -xzvf repbench-datasets-v1.tar.gz`. *The canonical way to obtain these datasets is generating them,* as the download server has limited capacity, no reliability guarantees, and loading `.npy` files from the internet amounts to arbitrary code execution on your machine. If you download the datasets, please also run the tests `cd tests; pytest` to check for corruption. For this to work, you must extract the file into this folder.

### Details

The repository is structured as follows:

The `qm9`, `ba10`, and `nmd18` folders contain the scripts used in generating the datasets. They also contain Jupyter Notebooks which were used to generate the stratified splits. These notebooks are intended to be read-only, as documentation.

In more detail, the dataset subfolders contain:
- `README.md` with further information
- `generate.sh` a shell script to generate the datasets
- `NAME-datasplits.txt` with the indices used for the `repbench` stratified subsets
- `iterate.py` which provides some basic ways to loop over all subsets
- `convert_NAME.py` to actually generate the datasets.
- A `jupyter` notebook detailing the stratification process (READ ONLY)

### Datasets

In `datasets/`, each file is a `cmlkit` dataset. 

Let `NAME` be the overall dataset name, `I` the outer split, `N` the training set size:
- `NAME-ot-I-N`: Training sets
- `NAME-ov-I`: Validation sets

The inner splits for HP optimisation can be accessed with the `.splits` property of the training datasets, which will return a list of length 10 where each entry is `[idx_train, idx_test]`.

If `datasets` is in `CML_DATASET_PATH`, all datasets can be loaded by name.

### Versioning

In case changes are made in the future, this will become a changelog.

- v1 (this, April 2020): Initial upload.
