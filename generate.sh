#!/bin/sh

mkdir datasets

cd ba10
sh generate.sh
cd ..

cd qm9
sh generate.sh
cd ..

cd nmd18
sh generate.sh
cd ..

cd tests
pytest